<?php

/*
 *  brainchild > config.php
 *  =========================
 *
 *  :copyright: (c) 2016-17 by BASH Labs Private Limited.
 *  :license: BASH Labs Private License. See LICENSE.md for more details.
 */

return [

    'displayErrorDetails' => true,

    'config' => [

        'displayErrorDetails' => true,

        'view' => [

            'path' => __DIR__ . '/templates/',
            'twig' => [

                'cache' => false

            ]
        ],

        'pdo' => [

            'engine' => 'mysql',
            'dbserver' => 'localhost',
            'dbname' => 'bc_mk17',
            'dbuser' => 'root',
            'dbpass' => 'iamyouth',
            'charset' => 'utf8',
            'options' => [

                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES   => true

            ]
        ]
    ]
];

