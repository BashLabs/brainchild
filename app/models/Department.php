<?php

/*
 *  brainchild > Depts.php
 *  =========================
 *
 *  :copyright: (c) 2016-17 by BASH Labs Private Limited.
 *  :license: BASH Labs Private License. See LICENSE.md for more details.
 */

class Department
{
    var $db = NULL;

    public function __construct(&$db)
    {
        $this->db = &$db;
    }

    public static function getDepartments($db)
    {
        $data = [];
        $result = $db->select('departments',"*");
        foreach ($result as $r){
            $temp = [];
            $user = $db->select('users','*',[
                'id'=>$r['user_id']
            ]);
            $temp = array_merge($temp,[
                'id'=>$r['id'],
                'name'=>$r['name'],
                'contact'=>$r['contact'],
                'co_name'=>$user[0]['name']
            ]);
            array_push($data, $temp);
        }
        return $result;
    }

    public static function getDepartmentsAdmin($db)
    {
        $data = [];
        $result = $db->select('departments',"*");
        foreach ($result as $r){
            $temp = [];
            $user = $db->select('users','*',[
                'id'=>$r['user_id']
            ]);
            $temp = array_merge($temp,[
                'id'=>$r['id'],
                'name'=>$r['name'],
                'contact'=>$r['contact'],
                'username'=>$user[0]['username'],
                'password'=>Registration::encrypt_decrypt("decrypt",$user[0]['password']),
                'co_name'=>$user[0]['name']
            ]);
            array_push($data, $temp);
        }
        return $data;
    }

    public static function getEvents($db, $dept)
    {
        $Parsedown = new Parsedown();
        $data = [];
        $result = $db->select('events',"*",[
            "dept" => $dept
        ]);
        foreach ($result as $r){
            $temp = [];

            $user = $db->select('users','*',[
                'id'=>$r['user_id']
            ]);

            $reg_count = $db->count('event_registration',[
                'event_id'=>$r['id']
            ]);
            if($r['fees']==0){
                $fee = "FREE";
            }else{
                $fee = "Rs.".$r['fees'];
            }
            $temp = array_merge($temp,[
                'id'=>$r['id'],
                'title'=>$r['title'],
                'type'=>EVENT_TYPE::getName(intval($r['type'])),
                'fee'=>$fee,
                'description' => $Parsedown->text($r['description']),
                'contact'=>$r['contact'],
                'count'=>$reg_count,
                'venue'=>$r['venue'],
                'prize'=>$r['prize'],
                'imgurl'=>$r['imgurl'],
                'stat'=>intval($r['status']),
                'status'=>EVENT_STATUS::getName(intval($r['status'])),
                'color'=>EVENT_STATUS::getColor(intval($r['status'])),
                'co_name'=>$user[0]['name'],
                'username'=>$user[0]['username']
            ]);
            array_push($data, $temp);
        }
        return $data;
    }

    public static function getEventsDepartmentVerified($db, $dept)
    {
        $data = [];
        $result = $db->select('events',"*",[
            'AND' => [
                "dept" => $dept,
                "status[>]" => EVENT_STATUS::$APPROVAL_PENDING
            ]
        ]);
        foreach ($result as $r){
            $temp = [];
            $user = $db->select('users','*',[
                'id'=>$r['user_id']
            ]);
            $temp = array_merge($temp,[
                'id'=>$r['id'],
                'title'=>$r['title'],
                'type'=>EVENT_TYPE::getName(intval($r['type'])),
                'fee'=>$r['fees'],
                'description'=>$r['description'],
                'contact'=>$r['contact'],
                'username'=>$user[0]['username'],
                'password'=>Registration::encrypt_decrypt("decrypt",$user[0]['password']),
                'co_name'=>$user[0]['name']
            ]);
            array_push($data, $temp);
        }
        return $data;
    }

    public static function getEventsDepartment($db, $dept)
    {
        $data = [];
        $result = $db->select('events',"*",[
            "dept" => $dept
        ]);
        foreach ($result as $r){
            $temp = [];
            $user = $db->select('users','*',[
                'id'=>$r['user_id']
            ]);
            $temp = array_merge($temp,[
                'id'=>$r['id'],
                'title'=>$r['title'],
                'type'=>EVENT_TYPE::getName(intval($r['type'])),
                'fee'=>$r['fees'],
                'description'=>$r['description'],
                'contact'=>$r['contact'],
                'username'=>$user[0]['username'],
                'password'=>Registration::encrypt_decrypt("decrypt",$user[0]['password']),
                'co_name'=>$user[0]['name']
            ]);
            array_push($data, $temp);
        }
        return $data;
    }

    public static function checkDept($db, $short){

        // Check if username already exists
        $check = $db->count('departments', [
            'id' => $short
        ]);

        if($check>0){
            return ['status'=>'fail', 'message'=>'Department already exists.'];
        }

        return ['status'=>'success'];

    }

    public static function getDept($db, $user_id){

        $dept = $db->select("departments", "*",[
            "user_id" => $user_id
        ]);

        if($dept[0]['id']!=''){
            return $dept[0]['id'];
        }
    }

    public static function addDept($db, $short, $name, $contact, $user_id){

        $val = $db->insert('departments',[
            "id" => $short,
            "name" => $name,
            "contact" => $contact,
            "user_id" => $user_id,
            "imgurl" => ''
        ]);

        return ['status'=>'success', 'message'=>'Department added.'];
    }

    public static function deleteDept($db, $short){

        $check = $db->count('departments', [
            'id' => $short
        ]);

        if($check==0){
            return ['status'=>'fail', 'message'=>'Department does not exists.'];
        }

        $dept = $db->select('departments','*',[
            'id' => $short
        ]);

        $db->delete('users',[
           'id' => $dept[0]['user_id']
        ]);

        $db->delete('departments',[
            'id' => $short
        ]);

        return ['status'=>'success','message'=>'Department deleted.'];

    }

    public static function addEvent($db, $dept, $title, $type, $fee, $contact, $user_id){

        $val = $db->insert('events',[
            "title" => $title,
            "type" => $type,
            "contact" => $contact,
            "user_id" => $user_id,
            "fees" => $fee,
            "dept" => $dept,
            "status" => EVENT_STATUS::$APPROVAL_PENDING,
            "sms_count" => 2,
            "venue" => "Will be Updated soon",
            "rules" => "",
            "description" => "",
            "imgurl" => ''
        ]);

        return ['status'=>'success', 'message'=>'Event added.'];
    }

    public static function deleteEvent($db, $id){

        $check = $db->count('events', [
            'id' => $id
        ]);

        if($check==0){
            return ['status'=>'fail', 'message'=>'Event does not exists.'];
        }

        $event = $db->select('events','*',[
            'id' => $id
        ]);

        $db->delete('users',[
            'id' => $event[0]['user_id']
        ]);

        $db->delete('events',[
            'id' => $id
        ]);

        return ['status'=>'success','message'=>'Event deleted.'];

    }

    public static function getStatistics($db, $dept){

        //KU registrations

        $ku_count = $db->count("event_registration","id",[

            "AND" => [
                "event_id"=> $db->select('events','id',[
                    "dept"=>$dept
                ]),
                "user_id"=> $db->select('users','id',[
                    "type"=>USER_TYPE::$STUDENT_INTERNAL
                ])
            ]
        ]);

        $ex_count = $db->count("event_registration","id",[

            "AND" => [
                "event_id"=> $db->select('events','id',[
                    "dept"=>$dept
                ]),
                "user_id"=> $db->select('users','id',[
                    "type"=>USER_TYPE::$STUDENT_EXTERNAL
                ])
            ]
        ]);

        $tech = $db->count("event_registration","id",[

            "event_id"=> $db->select("events",'id',[

                "AND" => [
                    "type" => EVENT_TYPE::$TECHNICAL,
                    "dept" => $dept
                ]

            ])

        ]);

        $ntech = $db->count("event_registration","id",[

            "event_id"=> $db->select("events",'id',[

                "AND" => [
                    "type" => EVENT_TYPE::$NONTECHNICAL,
                    "dept" => $dept
                ]

            ])

        ]);

        $work = $db->count("event_registration","id",[

            "event_id"=> $db->select("events",'id',[

                "AND" => [
                    "type" => EVENT_TYPE::$WORKSHOP,
                    "dept" => $dept
                ]

            ])

        ]);

        return [
            'ku'=>$ku_count,
            'ex'=>$ex_count,
            'tech'=>$tech,
            'ntech'=>$ntech,
            'work'=>$work
        ];


    }

}