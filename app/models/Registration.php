<?php

/*
 *  brainchild > Registration.php
 *  =========================
 *
 *  :copyright: (c) 2016-17 by BASH Labs Private Limited.
 *  :license: BASH Labs Private License. See LICENSE.md for more details.
 */

include_once "Enums.php";
include_once "OTP.php";

class Registration
{
    var $db = null;
    var $session = null;

    public static function encrypt_decrypt($action, $string) {
        $output = false;

        $encrypt_method = "AES-256-CBC";
        $secret_key = 'This is my secret key';
        $secret_iv = 'This is my secret iv';

        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        }
        else if( $action == 'decrypt' ){
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }

    public function __construct(&$db, &$session)
    {
        $this->db = &$db;
        $this->session = &$session;
    }

    public function internal($regno, $name, $email, $phone, $gender, $dept)
    {
        /*
         * Register internal participants
         */
        $regno = strtoupper($regno);

        // Check if valid Karunya id
        $karunya = $this->db->select('karunya',"*", [
            "regno" => $regno
        ]);

        if(count($karunya)){
            $a = 1;
        }else{
            return ['status'=>'fail','message'=>'Not a Karunya register number.'];
        }

        // Check if username already exists
        $check = $this->db->count('users', [
            'username' => $regno
        ]);

        if($check>0){
            return ['status'=>'fail', 'message'=>'Register number already exists.'];
        }

        // Check if phone number already exists
        $check = $this->db->count('users', [
            'phone' => $phone
        ]);

        if($check>0){
            return ['status'=>'fail', 'message'=>'Phone number already exists.'];
        }
        // Generate random 6 digit number for OTP and store in OTP table.
        $pass = rand(1000,9999);
        // Insert into registration table
        $this->db->insert('users', [
            "username" => $regno,
            "password" => self::encrypt_decrypt("encrypt",$pass),
            "name" => $name,
            "email" => $email,
            "phone" => $phone,
            "dept" => $dept,
            "gender" => $gender,
            "college" => COLLEGE::$HOME,
            "ip" => (string)$_SERVER['REMOTE_ADDR'],
            "type" => USER_TYPE::$STUDENT_INTERNAL,
            "status" => USER_STATUS::$VERIFIED
        ]);

        $otp = new OTP($this->db, $this->session);
        $check = $otp->Send_OTP_Registration($phone, $pass);

        return ['status'=>'success', 'message'=>'Check SMS for password.'];

    }

    public function external($regno, $name, $email, $phone, $college, $gender, $dept)
    {
        /*
         * Register external participants
         */

        $regno = strtoupper($regno);

        // Check if username already exists
        $check = $this->db->count('users', [
            'username' => $regno
        ]);

        if($check>0){
            return ['status'=>'fail', 'message'=>'Register number already exists.'];
        }

        // Check if phone number already exists
        $check = $this->db->count('users', [
            'phone' => $phone
        ]);

        if($check>0){
            return ['status'=>'fail', 'message'=>'Phone number already exists.'];
        }

        // Check if email already exists
        $check = $this->db->count('users', [
            'email' => $email
        ]);

        if($check>0){
            return ['status'=>'fail', 'message'=>'Email already exists.'];
        }

        // Generate random 6 digit number for OTP and store in OTP table.
        $pass = rand(1000,9999);
        // Insert into registration table
        $this->db->insert('users', [
            "username" => $regno,
            "password" => self::encrypt_decrypt("encrypt", $pass),
            "name" => $name,
            "email" => $email,
            "phone" => $phone,
            "dept" => $dept,
            "gender" => $gender,
            "college" => $college,
            "ip" => (string)$_SERVER['REMOTE_ADDR'],
            "type" => USER_TYPE::$STUDENT_EXTERNAL,
            "status" => USER_STATUS::$VERIFIED
        ]);

        $otp = new OTP($this->db, $this->session);
        $check = $otp->Send_OTP_Registration($phone, $pass);

        return ['status'=>'success', 'message'=>'Check SMS for password.'];

    }

    public static function coordinator($db, $regno, $name, $pass, $email, $phone, $gender, $type){

        $regno = strtoupper($regno);

        // Check if username already exists
        $check = $db->count('users', [
            'username' => $regno
        ]);

        if($check>0){
            return ['status'=>'fail', 'message'=>'Register number already exists.'];
        }

        // Check if phone number already exists
        $check = $db->count('users', [
            'phone' => $phone
        ]);

        if($check>0){
            return ['status'=>'fail', 'message'=>'Phone number already exists.'];
        }

        $val = $db->insert("users", [
            "username" => $regno,
            "password" => self::encrypt_decrypt("encrypt", $pass),
            "name" => $name,
            "email" => $email,
            "phone" => $phone,
            "dept" => "COORD",
            "gender" => $gender,
            "college" => COLLEGE::$HOME,
            "ip" => (string)$_SERVER['REMOTE_ADDR'],
            "type" => $type,
            "status" => USER_STATUS::$IDNOTISSUED
        ]);

        return ['status'=>'success', 'user'=>$val];

    }
}