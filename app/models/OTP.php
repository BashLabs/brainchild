<?php

/*
 *  brainchild > OTP.php
 *  =========================
 *
 *  :copyright: (c) 2016-17 by BASH Labs Private Limited.
 *  :license: BASH Labs Private License. See LICENSE.md for more details.
 */

include_once "Enums.php";

class OTP
{
    /*
     * One Time Password/Pin helper class
     */

    var $db = NULL;
    var $session = null;

    public function __construct(&$db, &$session)
    {
        $this->db = &$db;
        $this->session = &$session;

    }

    public static function Send_OTP_Registration($phone, $pass)
    {
        /*
         * Send OTP for registration to phone number
         */
        return 1;
        $request =""; //initialise the request variable
        $param[method]= "sendMessage";
        $param[send_to] = "91".$phone;
        $param[msg] = "Your password is ".$pass." ~ Mindkraft Team";
        $param[userid] = "20000xxxxx";
        $param[password] = "xxxxxxxx";
        $param[v] = "1.1";
        $param[msg_type] = "TEXT"; //Can be "FLASH”/"UNICOD
        $param[auth_scheme] = "PLAIN";
        foreach($param as $key=>$val) {
            $request.= $key."=".urlencode($val);
            $request.= "&";
        }
        $request = substr($request, 0, strlen($request)-1);
        //remove final (&) sign from the request
        $url = "http://enterprise.smsgupshup.com/GatewayAPI/rest?".$request;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);

        // TODO: Integrate SMS gateway
        // send_otp($phone, $otp);
        return 1;


    }

    public function Resend_OTP($phone)
    {
        /*
         * Resend OTP
         */

        // Get user details with phone number.
        $user = $this->db->select('users', '*', [
            "phone" => $phone
        ]);

        if(count($user)){

            // Generate random 6 digit number for OTP and store in OTP table.
            $otp = rand(1000,9999);
            $user = $this->db->update('users', [
                "password"=> $otp
            ], [
                "phone" == $phone
            ]);
            // TODO: Integrate SMS gateway
            // send_otp($phone, $otp);

            return ['status'=>'success', 'message'=>'Password Changed. You should get your password via sms.'];

        }else{
            return ['status'=>'fail', 'message'=>'Invalid Phone number.'];
        }

    }
}