<?php

/******************************************************************************
 *######                 BRAINCHILD(c)  - Mindkraft 2017                ######*
 ******************************************************************************/

/*
 *  brainchild > routes.php
 *  =========================
 *
 *  :copyright: (c) 2016-17 by BASH Labs Private Limited.
 *  :license: BASH Labs Private License. See LICENSE.md for more details.
 *
 *  :authors: Abiyouth Manickam, Amith Raji
 */


/******************************************************************************
 *#####################        IMPORTS GO HERE        ########################*
 ******************************************************************************/


include_once "models/Enums.php";
include_once "models/Registration.php";
include_once "models/OTP.php";
include_once "models/UserManager.php";
include_once "models/Department.php";
include_once "models/Event.php";
include_once "models/Admin.php";


/******************************************************************************
 *#####################       PAGE RENDER ROUTES        ######################*
 ******************************************************************************/


$app->get('/', function($request, $response){
    /*
     * Home page
     */
    $session = new RKA\Session();

    // If user logged in display Username and My Events redirect.
    if($session->get('active', false)){
        $user = $session->get('user');
        $name = $session->get('name');

        return $this->view->render($response, 'website/home.twig', [
            'data' => [
                'user'=>$user,
                'name'=>$name
            ]
        ]);
    }

    // Render page when not logged in.
    return $this->view->render($response, 'website/home.twig');

})->setName('page_home');


$app->get('/login/', function($request, $response){
    /*
     * Login page
     */
    $session = new RKA\Session();

    // If user logged in redirect to home.
    if($session->get('active', false)){
        return $response->withRedirect($this->router->pathFor('page_home'));
    }

    // Render page when not logged in.
    return $this->view->render($response, 'website/login.twig');

})->setName('page_login');

$app->get('/register/', function($request, $response){
    /*
     * Login page
     */
    $session = new RKA\Session();

    // If user logged in redirect to home.
    if($session->get('active', false)){
        return $response->withRedirect($this->router->pathFor('page_home'));
    }

    // Render page when not logged in.
    return $this->view->render($response, 'website/register.twig');

})->setName('page_register');


$app->get('/logout/', function($request, $response){
    /*
     * Logout page
     */

    // Destroy session and Logout.
    RKA\Session::destroy();

    // Redirect to home.
    return $response->withRedirect($this->router->pathFor('page_home'));

})->setName('page_logout');


$app->get('/myevents/',function($request, $response){
    /*
     * Redirect to relevant pages.
     */

    $session = new RKA\Session();

    // If user not logged in redirect to Login.
    if(!$session->get('active', false)){
        return $response->withRedirect($this->router->pathFor('page_login'));
    }

    $user_type = $session->get('type', 0);

    if ($user_type == USER_TYPE::$ADMIN){
        return $response->withRedirect($this->router->pathFor('admin_dashboard'));
    } elseif ($user_type == USER_TYPE::$DEPARTMENT) {
        return $response->withRedirect($this->router->pathFor('department_dashboard'));
    } elseif ($user_type == USER_TYPE::$EVENT_CO) {
        return $response->withRedirect($this->router->pathFor('event_dashboard'));
    } else {
        return $response->withRedirect($this->router->pathFor('page_home'));
    }



})->setName('page_myevents');


$app->get('/events/',function($request, $response){
    /*
     * Departments page
     */
    // $department = new Department($this->db);
    // $depts = $department->getDepartments();
    $depts = Department::getDepartments($this->db);
    return $this->view->render($response, 'website/depts.twig', ["depts"=>$depts]);

})->setName('page_depts');


$app->get('/events/{dept}/',function($request, $response, $args){

    // $event = new Event($this->db);
    // $dept = $args['dept'];
    // $data = $event->getEvents($dept);
    $events = Department::getEventsDepartmentVerified($this->db, $args['dept']);

    return $this->view->render($response, 'website/events.twig', ['name'=>$args['dept'], "events"=>$events] );

})->setName('page_events_dept');


$app->get('/games/',function($request, $response){
  // $events = Department::getEventsDepartmentVerified($this->db, $args['dept']);

  return $this->view->render($response, 'website/games.twig');
})->setName('page_games');


$app->get('/workshops/',function($request, $response){

})->setName('page_workshops');


$app->get('/harmony/',function($request, $response){

})->setName('page_harmony');


/******************************************************************************
 *####################         AJAX API ROUTES          ######################*
 ******************************************************************************/


$app->post('/api/login/', function($request, $response) {
    /*
     * AJAX : Login
     */

    $session = new RKA\Session();
    $login_manger = new UserManager($this->db, $session);

    $body = $response->getBody();

    // Check if already logged in.
    if($session->get('active', false))
    {
        $body->write(json_encode(['status'=>'fail', 'message'=>'Already logged in.']));
        return $response->withHeader('Content-Type', 'application/json')->withBody($body);
    }

    $data = $request->getParsedBody();

    $user = $data['username'] or NULL;
    $pass = $data['password'] or NULL;

    if($user == NULL or $pass == NULL)
    {
        $body->write(json_encode(['status'=>'fail', 'message'=>'Empty values.']));
        return $response->withHeader('Content-Type', 'application/json')->withBody($body);
    }

    // Login happens here. Check UserManager.php.
    $result = $login_manger->loginUser($user, $pass);

    $body->write(json_encode($result));
    return $response->withHeader('Content-Type', 'application/json')->withBody($body);

})->setName('api_login');


$app->post('/api/reg/internal/', function($request, $response) {
    /*
     * AJAX : Internal participant registration
     *
     * Input Data
     *  {
     *      "regno": "UL13CS014",
     *      "name": "Abiyouth",
     *      "pass": "password",
     *      "phone": "9600424320",
     *      "gender": "M",
     *      "dept": "CSE"
     *   }
     *
     * Output Data
     *  {
     *      "status": "success"/"fail",
     *      "message": "Relevant Message"
     *  }
     */

    $session = new \RKA\Session();
    $registration = new Registration($this->db, $session);

    $body = $response->getBody();

    // Check if already logged in.
    if($session->get('active', false))
    {
        $body->write(json_encode(['status'=>'fail', 'message'=>'Already logged in.']));
        return $response->withHeader('Content-Type','application/json')->withBody($body);
    }

    $data = $request->getParsedBody();

    $regno = $data['regno'] or NULL;
    $name = $data['name'] or NULL;
    $email = $data['email'];
    $gender = $data['gender'] or NULL;
    $phone = $data['phone'] or NULL;
    $dept = $data['dept'] or NULL;

    if( $regno == NULL or $name == NULL or $gender == NULL or $phone == NULL or $dept == NULL)
    {
        $body->write(json_encode(['status'=>'fail', 'message'=>'Empty Values']));
        return $response->withHeader('Content-Type','application/json')->withBody($body);
    }

    // Register happens here. Check Register.php.
    $result = $registration->internal($regno, $name, $email ,$phone, $gender, $dept);

    $body->write(json_encode($result));

    return $response->withHeader('Content-Type','application/json')->withBody($body);

})->setName('api_reg_internal');


$app->post('/api/reg/external/', function($request, $response) {
    /*
     * AJAX : External participant registration
     *
     * Input Data
     *  {
     *      "username": "kiru",
     *      "name": "Kiruthika",
     *      "pass": "password",
     *      "email": "kiru@gmail.com"
     *      "phone": "8802200339",
     *      "gender": "F",
     *      "dept": "ECE",
     *      "college": "Stanes College"
     *  }
     *
     * Output Data
     *  {
     *      "status": "success"/"fail",
     *      "message": "Relevant Message"
     *  }
     */

    $session = new \RKA\Session();
    $registration = new Registration($this->db, $session);

    $body = $response->getBody();

    // Check if already logged in.
    if($session->get('active', false))
    {
        $body->write(json_encode(['status'=>'fail', 'message'=>'Already logged in.']));
        return $response->withHeader('Content-Type','application/json')->withBody($body);
    }

    $data = $request->getParsedBody();

    $regno = $data['username'] or NULL;
    $name = $data['name'] or NULL;
    $email = $data['email'] or NULL;
    $college = $data['college'] or NULL;
    $gender = $data['gender'] or NULL;
    $phone = $data['phone'] or NULL;
    $dept = $data['dept'] or NULL;

    if( $regno == NULL or $name == NULL or $gender == NULL or $phone == NULL or $dept == NULL or $college == NULL or $email == NULL )
    {
        $body->write(json_encode(['status'=>'fail', 'message'=>'Empty Values']));
        return $response->withHeader('Content-Type','application/json')->withBody($body);
    }
    // Registration happens here. Check Register.php.
    $result = $registration->external($regno, $name, $email, $phone, $college, $gender, $dept);

    $body->write(json_encode($result));

    return $response->withHeader('Content-Type','application/json')->withBody($body);

})->setName('api_reg_external');


$app->post('/api/resend/otp/', function($request, $response) {
    /*
     * AJAX : Resend OTP pin
     *
     * Input Data
     *  {
     *      "phone": "9600424320"
     *  }
     *
     * Output Data
     *  {
     *       "status": "success"/"fail",
     *       "message": "relevant message"
     *  }
     */

    $session = new \RKA\Session();
    $otp = new OTP($this->db, $session);

    $body = $response->getBody();
    $data = $request->getParsedBody();

    $phone = $data['phone'] or NULL;

    if( $phone == NULL )
    {
        $body->write(json_encode(['status'=>'fail', 'message'=>'Empty Values']));
        return $response->withHeader('Content-Type','application/json')->withBody($body);
    }

    // OTP verification happens here. Check OTP.php.
    $result = $otp->Resend_OTP($phone);

    $body->write(json_encode($result));
    return $response->withHeader('Content-Type','application/json')->withBody($body);

})->setName('api_resend_otp');


$app->post('/api/reg/event/', function($request, $response, $args) {

})->setName('api_reg_event');


$app->post('/api/reg/game/', function($request, $response, $args) {

})->setName('api_reg_game');


$app->post('/api/submit/feedback/', function($request, $response, $args) {

})->setName('api_submit_feedback');


$app->post('/api/submit/harmony/', function($request, $response, $args) {

})->setName('api_submit_harmony');


$app->post('/api/reset/password/', function($request, $response, $args) {

})->setName('api_reset_password');


/******************************************************************************
 *###################         MOBILE API ROUTES          #####################*
 ******************************************************************************/


/******************************************************************************
 *###################        DEBUG/SPECIAL ROUTES         ####################*
 ******************************************************************************/


$app->get('/debug/secret/{data}/', function ($request, $response, $args) {
    /*
     * I wont explain :P
     */

    return Registration::encrypt_decrypt("encrypt", $args["data"]);

})->setName("debug_ed");

$app->get('/debug/tables/{what}/{god}/', function($request, $response, $args) {
    /*
     * Create/Drop Tables
     */

    if(Registration::encrypt_decrypt("encrypt", $args['god'])==="c1RiYnlWTXNwUW5ETDcremF2clN0dz09"){

        if($args["what"]==="drop") {

            $this->db->query("DROP TABLE accomodation;");
            $this->db->query("DROP TABLE departments");
            $this->db->query("DROP TABLE event_registration");
            $this->db->query("DROP TABLE events");
            $this->db->query("DROP TABLE feedbacks");
            $this->db->query("DROP TABLE game_registration");
            $this->db->query("DROP TABLE karunya");
            $this->db->query("DROP TABLE notifications");
            $this->db->query("DROP TABLE prize");
            $this->db->query("DROP TABLE settings");
            $this->db->query("DROP TABLE users");

            return "Done Master :(";

        }else if($args["what"]==="create") {

            $this->db->query("CREATE TABLE accomodation
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_id VARCHAR(30),
    status INT(11),
    accom_time DATETIME DEFAULT CURRENT_TIMESTAMP
);");
            $this->db->query("CREATE TABLE departments
(
    id VARCHAR(10) PRIMARY KEY NOT NULL,
    name VARCHAR(50),
    contact VARCHAR(200),
    user_id INT(11),
    imgurl VARCHAR(200)
);");
            $this->db->query("CREATE TABLE event_registration
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_id INT(11),
    event_id INT(11),
    status INT(11),
    registration_time DATETIME DEFAULT CURRENT_TIMESTAMP
);");
            $this->db->query("CREATE TABLE events
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    title VARCHAR(100) NOT NULL,
    description TEXT,
    rules TEXT,
    venue TEXT,
    contact TEXT,
    prize INT(11),
    fees INT(11),
    imgurl TEXT,
    dept VARCHAR(20),
    type INT(11),
    status INT(11),
    user_id INT(11),
    sms_count INT(11),
    created_time DATETIME DEFAULT CURRENT_TIMESTAMP
);");
            $this->db->query("CREATE TABLE feedbacks
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_id INT(11),
    feedback TEXT,
    stars INT(11),
    harmony_msg TEXT,
    fb_time DATETIME DEFAULT CURRENT_TIMESTAMP
);");
            $this->db->query("CREATE TABLE game_registration
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_id INT(11),
    game_id INT(11),
    turns INT(11),
    status INT(11),
    registration_time DATETIME DEFAULT CURRENT_TIMESTAMP
);");
            $this->db->query("CREATE TABLE karunya
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    regno VARCHAR(12) NOT NULL,
    name VARCHAR(150),
    email VARCHAR(150) NOT NULL
);
CREATE UNIQUE INDEX karunya_email_uindex ON karunya (email);
CREATE UNIQUE INDEX karunya_regno_uindex ON karunya (regno);");
            $this->db->query("CREATE TABLE notifications
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_id INT(11),
    msg TEXT,
    status INT(11),
    not_time DATETIME DEFAULT CURRENT_TIMESTAMP
);");
            $this->db->query("CREATE TABLE prize
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_id INT(11) NOT NULL,
    event_id INT(11),
    place INT(11),
    status INT(11),
    ammount INT(11),
    win_time DATETIME DEFAULT CURRENT_TIMESTAMP
);");
            $this->db->query("CREATE TABLE settings
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    value INT(11) NOT NULL
);");
            $this->db->query("CREATE TABLE users
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    username VARCHAR(30) NOT NULL,
    password VARCHAR(50) NOT NULL,
    name VARCHAR(100) NOT NULL,
    email VARCHAR(100),
    phone VARCHAR(10) NOT NULL,
    dept VARCHAR(10) NOT NULL,
    gender VARCHAR(6) NOT NULL,
    college VARCHAR(200) NOT NULL,
    status INT(11) NOT NULL,
    ip VARCHAR(16) NOT NULL,
    registration_time DATETIME DEFAULT CURRENT_TIMESTAMP,
    type INT(11) NOT NULL
);
CREATE UNIQUE INDEX registrations_phone_uindex ON users (phone);");

            $this->db->insert("users",[
                "username" => "ADMIN",
                "password" => "UXQzd25GV20vZE1wQlZ0ZFVGbFFLdz09",
                "name" => "ADMINISTRATOR",
                "email" => "abiyouth@live.com",
                "phone" => "9600424320",
                "dept" => "ADMIN",
                "gender" => "GOD",
                "college" => COLLEGE::$HOME,
                "status" => USER_STATUS::$PAYED,
                "ip" => "127.0.0.1",
                "type" => USER_TYPE::$ADMIN
            ]);

            return "Don't forget to import KU details.";

        }else{
            return "You are still dumb";
        }
    }

})->setName('debug_create_table');

$app->get('/debug/import/karunya/', function ($request, $response, $args) {
    /*
     * Import KU details.
     */
    return $this->view->render($response, 'debug/import.twig');

})->setName("debug_import_ku");

$app->post('/debug/import/karunya/', function ($request, $response, $args) {
    /*
     * Import KU details.
     */

    $data = $request->getParsedBody();

//    $_FILES['excel']
    $appPath = dirname(__FILE__) ;
//    return $appPath;
    $storage = new \Upload\Storage\FileSystem($appPath . '/uploads');
        $file = new \Upload\File('excel', $storage);
//    $newFilename = uniqid();
//    $file->setName($newFilename);

        // Validate file
        // MimeType List => http://www.webmaster-toolkit.com/mime-types.shtml

//        $file->addValidations([
////        new \Upload\Validation\Mimetype(['text/csv']),
//            new \Upload\Validation\Size('12M')
//        ]);
//        try {
//            $message = [
//                'name' => $file->getNameWithExtension(),
//                'mime' => $file->getMimetype(),
//                'size' => $file->getSize(),
//                'md5' => $file->getMd5(),
//                'dimensions' => $file->getDimensions()
//            ];
//            $file->upload();
//        } catch (\Exception $e) {
//            //$this->errors = $file->getErrors();
//        }
    return "HEHE";

})->setName("debug_import_ku_post");


/******************************************************************************
 *###################         BRAINCHILD ROUTES          #####################*
 ******************************************************************************/


/*#############################################################################
                                All Hail the ADMIN
 *############################################################################*/


$app->get('/admin/dashboard/', function ($request, $response, $args){
    /*
     * Admin Dashboard
     */
    $session = new RKA\Session();

    // Check if admin
    if(intval($session->get('type', false))!= USER_TYPE::$ADMIN)
    {
        return $response->withRedirect($this->router->pathFor('page_home'));
    }

    $stats = Admin::getStat($this->db);

    $data = ['stats'=>$stats, 'page_title'=>'Dashboard', 'user'=>'Admin', 'user_type'=>'MK Administrator',
    'header_description'=>'MindKraft 2017 Overview', 'event_name'=>'MindKraft'];

    return $this->view->render($response, 'brainchild/admin/dashboard.twig', $data);

})->setName("admin_dashboard");


$app->get('/admin/manage/', function ($request, $response, $args){

    $session = new RKA\Session();

    // Check if admin
    if(intval($session->get('type', false))!= USER_TYPE::$ADMIN)
    {
        return $response->withRedirect($this->router->pathFor('page_home'));
    }

    $depts = Department::getDepartmentsAdmin($this->db);

    $data = ['depts'=>$depts,'ku_reg'=>90, 'page_title'=>'Manage', 'user'=>'Admin', 'user_type'=>'MK Administrator',
        'header_description'=>'Manage Departments and Special Users', 'event_name'=>'MindKraft'];

    return $this->view->render($response, 'brainchild/admin/manage.twig', $data);

})->setName("admin_manage");


$app->get('/admin/departments/', function ($request, $response, $args){

    $session = new RKA\Session();

    // Check if admin
    if(intval($session->get('type', false))!= USER_TYPE::$ADMIN)
    {
        return $response->withRedirect($this->router->pathFor('page_home'));
    }

    $depts = Department::getDepartments($this->db);

    if($session->get('dept', false))
    {
        $events = Department::getEvents($this->db, $session->get('dept',""));

        $data = ['depts'=>$depts, 'events'=>$events, 'page_title'=>'Departments', 'user'=>'Admin', 'user_type'=>'MK Administrator',
            'header_description'=>'Department Event Details', 'event_name'=>'MindKraft'];

        return $this->view->render($response, 'brainchild/admin/departments.twig', $data);
    }


    $data = ['depts'=>$depts, 'page_title'=>'Departments', 'user'=>'Admin', 'user_type'=>'MK Administrator',
        'header_description'=>'Department Event Details', 'event_name'=>'MindKraft'];

    return $this->view->render($response, 'brainchild/admin/departments.twig', $data);

})->setName("admin_departments");


$app->get('/admin/registrations/', function ($request, $response, $args){

    $session = new RKA\Session();

    // Check if admin
    if(intval($session->get('type', false))!= USER_TYPE::$ADMIN)
    {
        return $response->withRedirect($this->router->pathFor('page_home'));
    }

    $data = ['ku_reg'=>90, 'page_title'=>'Dashboard', 'user'=>'Admin', 'user_type'=>'MK Administrator',
        'header_description'=>'MindKraft 2017 Overview', 'event_name'=>'MindKraft'];

    return $this->view->render($response, 'brainchild/admin/departments.twig', $data);

})->setName("admin_registrations");


$app->get('/admin/events/', function ($request, $response, $args){

    $session = new RKA\Session();

    // Check if admin
    if(intval($session->get('type', false))!= USER_TYPE::$ADMIN)
    {
        return $response->withRedirect($this->router->pathFor('page_home'));
    }

})->setName("admin_events");


/*#############################################################################
                                Departments Roll
 *############################################################################*/


$app->get('/department/dashboard/', function ($request, $response, $args){

    $session = new RKA\Session();

    // Check if admin
    if($session->get('type', false)!= USER_TYPE::$DEPARTMENT)
    {
        return $response->withRedirect($this->router->pathFor('page_home'));
    }

    $dept = Department::getDept($this->db, $session->get('id',0));
    $stats = Department::getStatistics($this->db, $dept);

    $data = ['ku_reg'=>90, 'user_type'=>USER_TYPE::getType($session->get("type", 5)), 'event_name'=>'MindKraft', 'stats'=>$stats];
    $data['user'] = $session->get("user","");
    $data['page_title'] = 'Dashboard';
    $data['header_description'] = 'Department Overview';

    return $this->view->render($response, 'brainchild/department/dashboard.twig', $data);

})->setName("department_dashboard");


$app->get('/department/details/', function ($request, $response, $args){

    $session = new RKA\Session();

    // Check if admin
    if($session->get('type', false)!= USER_TYPE::$DEPARTMENT)
    {
        return $response->withRedirect($this->router->pathFor('page_home'));
    }

    $data = ['ku_reg'=>90, 'user_type'=>USER_TYPE::getType($session->get("type", 5)), 'event_name'=>'MindKraft'];
    $data['user'] = $session->get("user","");
    $data['page_title'] = 'Details';
    $data['header_description'] = 'View and Update your department details.';

    return $this->view->render($response, 'brainchild/department/details.twig', $data);

})->setName("department_details");


$app->get('/department/manage/', function ($request, $response, $args){

    $session = new RKA\Session();

    // Check if admin
    if($session->get('type', false)!= USER_TYPE::$DEPARTMENT)
    {
        return $response->withRedirect($this->router->pathFor('page_home'));
    }

    $dept = Department::getDept($this->db, $session->get('id',0));

    $events = Department::getEventsDepartment($this->db, $dept);
    $data = ['ku_reg'=>90, 'user_type'=>USER_TYPE::getType($session->get("type", 5)), 'event_name'=>'MindKraft', 'events'=>$events];
    $data['user'] = $session->get("user","");
    $data['page_title'] = 'Manage';
    $data['header_description'] = 'Manage Events under your department.';

    return $this->view->render($response, 'brainchild/department/manage.twig', $data);

})->setName("department_manage");


$app->get('/department/events/', function ($request, $response, $args){

    $session = new RKA\Session();

    // Check if admin
    if($session->get('type', false)!= USER_TYPE::$DEPARTMENT)
    {
        return $response->withRedirect($this->router->pathFor('page_home'));
    }
    $dept = Department::getDept($this->db, intval($session->get("id",0)));
    $events = Department::getEvents($this->db, $dept);

    $data = ['ku_reg'=>90, 'user_type'=>USER_TYPE::getType($session->get("type", 5)), 'event_name'=>'MindKraft', "events"=>$events];
    $data['user'] = $session->get("user","");
    $data['page_title'] = 'Dashboard';
    $data['header_description'] = 'Department Overview';

    return $this->view->render($response, 'brainchild/department/events.twig', $data);

})->setName("department_events");


/*#############################################################################
                                Events ? OK!
 *############################################################################*/


$app->get('/event/dashboard/', function ($request, $response, $args){

    $session = new RKA\Session();

    // Check if admin
    if($session->get('type', false)!= USER_TYPE::$EVENT_CO)
    {
        return $response->withRedirect($this->router->pathFor('page_home'));
    }

    $event = Event::getEventDetails($this->db, intval($session->get("id",0)));

    $regis = Event::registrations($this->db, $event['id']);

    $count = Event::getEventRegistrationCount($this->db, $event['id']);

    $data = ['count'=>$count, 'regis'=>$regis, 'user_type'=>USER_TYPE::getType($session->get("type", 5)), 'event_name'=>'MindKraft'];
    $data['user'] = $session->get("user", "");
    $data['page_title'] = 'Dashboard';
    $data['header_description'] = 'Event Overview';

    return $this->view->render($response, 'brainchild/event/dashboard.twig', $data);

})->setName("event_dashboard");


$app->get('/event/manager/', function ($request, $response, $args){

    $session = new RKA\Session();

    // Check if admin
    if($session->get('type', false)!= USER_TYPE::$EVENT_CO)
    {
        return $response->withRedirect($this->router->pathFor('page_home'));
    }

    $data = ['ku_reg'=>90, 'user_type'=>USER_TYPE::getType($session->get("type", 5)), 'event_name'=>'MindKraft'];
    $data['user'] = $session->get("user","");
    $data['page_title'] = 'Dashboard';
    $data['header_description'] = 'Event Overview';

    return $this->view->render($response, 'brainchild/event/manager.twig');

})->setName("event_manager");


$app->get('/event/prize/', function ($request, $response, $args){

    $session = new RKA\Session();

    // Check if admin
    if($session->get('type', false)!= USER_TYPE::$EVENT_CO)
    {
        return $response->withRedirect($this->router->pathFor('page_home'));
    }

    $data = ['ku_reg'=>90, 'user_type'=>USER_TYPE::getType($session->get("type", 5)), 'event_name'=>'MindKraft'];
    $data['user'] = $session->get("user","");
    $data['page_title'] = 'Dashboard';
    $data['header_description'] = 'Event Overview';

    return $this->view->render($response, 'brainchild/event/prize.twig');

})->setName("event_prize");


$app->get('/event/details/', function ($request, $response, $args){

    $session = new RKA\Session();

    // Check if admin
    if($session->get('type', false)!= USER_TYPE::$EVENT_CO)
    {
        return $response->withRedirect($this->router->pathFor('page_home'));
    }

    $abi = Event::getEventDetails($this->db, intval($session->get("id",0)));
    $abi['s'] = $abi['status'];
    $abi['status']= EVENT_STATUS::getName(intval($abi['status']));
    $abi['color']= EVENT_STATUS::getColor(intval($abi['status']));
    $abi['type']= EVENT_TYPE::getName(intval($abi['type']));

    $data = ['details'=>$abi, 'user_type'=>USER_TYPE::getType($session->get("type", 5)), 'event_name'=>'MindKraft'];
    $data['user'] = $session->get("user","");
    $data['page_title'] = "Event Details";
    $data['header_description'] = 'Change Event details';

    return $this->view->render($response, 'brainchild/event/details.twig', $data);

})->setName("event_details");


/******************************************************************************
 *###################       BRAINCHILD AJAX ROUTES       #####################*
 ******************************************************************************/


$app->post('/admin/ajax/set_department/', function ($request, $response, $args){

    $session = new RKA\Session();
    $body = $response->getBody();

//    // Check if admin
    if($session->get('type', false)!= USER_TYPE::$ADMIN) {
        $body->write(json_encode(['status'=>'fail', 'message'=>'Not Admin']));
        return $response->withHeader('Content-Type', 'application/json')->withBody($body);
    }

    $data = $request->getParsedBody();

    $short = $data['dept'] or NULL;

    $session->set("dept", $short);

    $body->write(json_encode(['status'=>'success', 'message'=>'Done']));
    return $response->withHeader('Content-Type', 'application/json')->withBody($body);

})->setName("admin_ajax_set_department");

$app->post('/admin/ajax/add_department/', function ($request, $response, $args){
    /*
     * AJAX for Adding Department
     */
    $session = new RKA\Session();
    $body = $response->getBody();

//    // Check if admin
    if($session->get('type', false)!= USER_TYPE::$ADMIN) {
        $body->write(json_encode(['status'=>'fail', 'message'=>'Not Admin']));
        return $response->withHeader('Content-Type', 'application/json')->withBody($body);
    }

    $data = $request->getParsedBody();

    $short = $data['short_name'] or NULL;
    $name = $data['name'] or NULL;
    //$email = $data['email'];
    $co_name = $data['co_name'] or NULL;
    $phone = $data['phone'] or NULL;
    $user = $data['username'] or NULL;
    $pass = $data['password'] or NULL;

    $check = Department::checkDept($this->db, $short);
    if($check['status']==='fail') {

        $body->write(json_encode($check));
        return $response->withHeader('Content-Type', 'application/json')->withBody($body);
    }

    $check = Registration::coordinator($this->db, $user, $co_name, $pass, '', $phone, 'MF', USER_TYPE::$DEPARTMENT);
    if($check['status']==='fail') {

        $body->write(json_encode($check));
        return $response->withHeader('Content-Type', 'application/json')->withBody($body);

    }

    $check = Department::addDept($this->db, $short, $name, $phone, intval($check['user']));

    $body->write(json_encode($check));

    return $response->withHeader('Content-Type', 'application/json')->withBody($body);

})->setName('admin_ajax_add_department');

$app->post('/admin/ajax/approve/', function ($request, $response, $args){

    /*
     * AJAX for Adding Department
     */
    $session = new RKA\Session();
    $body = $response->getBody();

//    // Check if admin
    if($session->get('type', false)!= USER_TYPE::$ADMIN) {
        $body->write(json_encode(['status'=>'fail', 'message'=>'Not Admin']));
        return $response->withHeader('Content-Type', 'application/json')->withBody($body);
    }

    $data = $request->getParsedBody();

    $id = $data['id'] or NULL;

    $result = Event::approveEvent($this->db, $id);

    $body->write(json_encode($result));
    return $response->withHeader('Content-Type', 'application/json')->withBody($body);

})->setName("admin_ajax_approve");

$app->post('/admin/ajax/allow/', function ($request, $response, $args){

    /*
     * AJAX for Adding Department
     */
    $session = new RKA\Session();
    $body = $response->getBody();

//    // Check if admin
    if($session->get('type', false)!= USER_TYPE::$ADMIN) {
        $body->write(json_encode(['status'=>'fail', 'message'=>'Not Admin']));
        return $response->withHeader('Content-Type', 'application/json')->withBody($body);
    }

    $data = $request->getParsedBody();

    $id = $data['id'] or NULL;

    $result = Event::allowEditsEvent($this->db, $id);

    $body->write(json_encode($result));

    return $response->withHeader('Content-Type', 'application/json')->withBody($body);

})->setName("admin_ajax_allow_edit");

$app->post('/admin/ajax/delete_department/', function ($request, $response, $args){
    /*
     * AJAX Delete department
     */
    $session = new RKA\Session();
    $body = $response->getBody();

    // Check if admin
    if($session->get('type', false)!= USER_TYPE::$ADMIN) {
        $body->write(json_encode(['status'=>'fail', 'message'=>'Not Admin']));
        return $response->withHeader('Content-Type', 'application/json')->withBody($body);
    }

    $data = $request->getParsedBody();

    $short = $data['short_name'] or NULL;

    $check = Department::deleteDept($this->db, $short);

    $body->write(json_encode($check));
    return $response->withHeader('Content-Type', 'application/json')->withBody($body);

})->setName('admin_ajax_delete_department');

$app->post('/department/ajax/add_event/', function ($request, $response, $args){
    /*
     * AJAX for Adding Events
     */
    $session = new RKA\Session();
    $body = $response->getBody();

//    // Check if department coordinator
    if($session->get('type', false)!= USER_TYPE::$DEPARTMENT) {
        $body->write(json_encode(['status'=>'fail', 'message'=>'Not Department coordinator.']));
        return $response->withHeader('Content-Type', 'application/json')->withBody($body);
    }

    $data = $request->getParsedBody();

    $title = $data['title'] or NULL;
    $fee = $data['paid'] or NULL;
    $type = intval($data['type']) or NULL;
    $co_name = $data['co_name'] or NULL;
    $phone = $data['phone'] or NULL;
    $user = $data['username'] or NULL;
    $pass = $data['password'] or NULL;

    $check = Registration::coordinator($this->db, $user, $co_name, $pass, '', $phone, 'MF', USER_TYPE::$EVENT_CO);
    if($check['status']==='fail') {

        $body->write(json_encode($check));
        return $response->withHeader('Content-Type', 'application/json')->withBody($body);

    }

    $dept = Department::getDept($this->db, $session->get('id',0));

    $check = Department::addEvent($this->db, $dept, $title, $type, $fee, $phone, intval($check['user']));

    $body->write(json_encode($check));

    return $response->withHeader('Content-Type', 'application/json')->withBody($body);

})->setName('department_ajax_add_event');

$app->post('/department/ajax/delete_event/', function ($request, $response, $args){
    /*
     * AJAX Delete department
     */
    $session = new RKA\Session();
    $body = $response->getBody();

    // Check if admin
    if($session->get('type', false)!= USER_TYPE::$DEPARTMENT) {
        $body->write(json_encode(['status'=>'fail', 'message'=>'Not Admin']));
        return $response->withHeader('Content-Type', 'application/json')->withBody($body);
    }

    $data = $request->getParsedBody();

    $id = $data['id'] or NULL;

    $check = Department::deleteEvent($this->db, $id);

    $body->write(json_encode($check));
    return $response->withHeader('Content-Type', 'application/json')->withBody($body);

})->setName('department_ajax_delete_event');

$app->post('/event/update/details/', function ($request, $response, $args){

    $session = new RKA\Session();
    $body = $response->getBody();

    // Check if admin
    if($session->get('type', false)!= USER_TYPE::$EVENT_CO) {
        $body->write(json_encode(['status'=>'fail', 'message'=>'Not Admin']));
        return $response->withHeader('Content-Type', 'application/json')->withBody($body);
    }

    $data = $request->getParsedBody();

    $title = $data['title'] or NULL;
    $desc = $data['desc'] or NULL;
    $venue = $data['venue'] or NULL;
    $contact = $data['contact'] or NULL;
    $prize = $data['prize'] or NULL;
    $id = $data['id'] or NULL;

    $d = $this->db->update("events",[
       "title"=>$title,
        "description"=>$desc,
        "venue" => $venue,
        "contact"=>$contact,
        "prize" => intval($prize)
    ],[
        "id"=>intval($id)
    ]);

    if ($d!=""){
        $body->write(json_encode(['status'=>'success', 'message'=>'Not Admin']));
        return $response->withHeader('Content-Type', 'application/json')->withBody($body);
    }else{
        $body->write(json_encode(['status'=>'fail', 'message'=>'Not Admin']));
        return $response->withHeader('Content-Type', 'application/json')->withBody($body);
    }

})->setName('event_update');

/******************************************************************************
 *###################         BRAINCHILD THE END         #####################*
 ******************************************************************************/
