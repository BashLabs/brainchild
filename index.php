<?php

/*
 *  brainchild
 *  ==========
 *
 *  This is the brainchild web application.
 *
 *  This software is copyrighted, copy-protected and comes under BASH Labs's
 *  private licensing terms. No copying, modification or unauthorized viewing
 *  of this code is allowed by any means whatsoever.
 *
 *  :copyright: (c) 2016-2017 by BASH Labs Private Limited.
 *  :license: BASH Labs Private License. See LICENSE.md for more details.
 */


// Debug part - To be removed or commented during production

ini_set('display_errors', 0);
//error_reporting(E_ALL);

// Include plugins installed with composer.

require __DIR__ . '/vendor/autoload.php';

// Create new Slim app with custom config.

$config = require __DIR__ . '/app/config.php';
$app = new Slim\App( $config );

// Include middleware and routes.

require __DIR__ . '/app/middleware.php';
require __DIR__ . '/app/routes.php';

// Run application. See Slim docs for more details.

$app->run();

// All is Hell that doesn't ends well.
